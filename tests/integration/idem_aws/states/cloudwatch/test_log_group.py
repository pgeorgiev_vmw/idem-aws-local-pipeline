import copy
import uuid

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_log_group(hub, ctx):
    # Create log group
    log_group_temp_name = "idem-test-log-group-" + str(uuid.uuid4())
    tags = {"testKey1": "testValue1"}

    # Create log group with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.cloudwatch.log_group.present(
        test_ctx, name=log_group_temp_name, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.cloudwatch.log_group '{log_group_temp_name}'"
        in ret["comment"]
    )
    assert not ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    assert log_group_temp_name == resource.get("name")
    assert tags == resource.get("tags")
    assert "arn_known_after_present" == resource.get("arn")

    # Create real log group
    ret = await hub.states.aws.cloudwatch.log_group.present(
        ctx, name=log_group_temp_name, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert log_group_temp_name == resource.get("name")
    assert tags == resource.get("tags")

    tags = {"testKey2": "testValue2"}

    # Update log group with test flag
    ret = await hub.states.aws.cloudwatch.log_group.present(
        test_ctx, name=log_group_temp_name, resource_id=resource_id, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert log_group_temp_name == resource.get("name")
    assert list(tags.keys()) == resource.get("tags")

    # Update tags for log group
    ret = await hub.states.aws.cloudwatch.log_group.present(
        ctx, name=log_group_temp_name, resource_id=resource_id, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert log_group_temp_name == resource.get("name")
    assert tags == resource.get("tags")

    # Delete log group with test flag
    ret = await hub.states.aws.cloudwatch.log_group.absent(
        test_ctx, name=log_group_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.cloudwatch.log_group", name=log_group_temp_name
        )[0]
        in ret["comment"]
    )

    # Delete log group
    ret = await hub.states.aws.cloudwatch.log_group.absent(
        ctx, name=log_group_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.cloudwatch.log_group", name=log_group_temp_name
        )[0]
        in ret["comment"]
    )

    # Deleting the same instance again (deleted state) will not invoke delete on AWS side.
    ret = await hub.states.aws.cloudwatch.log_group.absent(
        ctx, name=log_group_temp_name, resource_id=resource_id
    )
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.cloudwatch.log_group", name=log_group_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret["old_state"] and not ret["new_state"]
